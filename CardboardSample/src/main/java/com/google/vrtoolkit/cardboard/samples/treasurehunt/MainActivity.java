/*
 * Copyright 2014 Google Inc. All Rights Reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.vrtoolkit.cardboard.samples.treasurehunt;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.vrtoolkit.cardboard.CardboardActivity;
import com.google.vrtoolkit.cardboard.CardboardView;
import com.google.vrtoolkit.cardboard.Eye;
import com.google.vrtoolkit.cardboard.HeadTransform;
import com.google.vrtoolkit.cardboard.Viewport;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.opengl.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;

/**
 * A Cardboard sample application.
 */
public class MainActivity extends CardboardActivity implements CardboardView.StereoRenderer, RecognitionListener {

    private static final String TAG = "VirtuAirMainActivity";

    private static final float Z_NEAR = 0.1f;
    private static final float Z_FAR = 100.0f;

    private static final float CAMERA_Z = 0.01f;
    private static final float TIME_DELTA = 0.3f;

    private static final float YAW_LIMIT = 0.12f;
    private static final float PITCH_LIMIT = 0.12f;

    private static final int COORDS_PER_VERTEX = 3;

    // We keep the light always position just above the user.
    private static final float[] LIGHT_POS_IN_WORLD_SPACE = new float[] { 0.0f, 2.0f, 0.0f, 1.0f };

    private final float[] lightPosInEyeSpace = new float[4];

    private FloatBuffer floorVertices;
    private FloatBuffer floorColors;
    private FloatBuffer floorNormals;

    private FloatBuffer cubeVertices;
    private FloatBuffer cubeColors;
    private FloatBuffer cubeFoundColors;
    private FloatBuffer cubeNormals;

    private int cubeProgram;
    private int floorProgram;

    private int cubePositionParam;
    private int cubeNormalParam;
    private int cubeColorParam;
    private int cubeModelParam;
    private int cubeModelViewParam;
    private int cubeModelViewProjectionParam;
    private int cubeLightPosParam;

    private int floorPositionParam;
    private int floorNormalParam;
    private int floorColorParam;
    private int floorModelParam;
    private int floorModelViewParam;
    private int floorModelViewProjectionParam;
    private int floorLightPosParam;

    private float[] modelCube;
    private float[] camera;
    private float[] view;
    private float[] headView;
    private float[] modelViewProjection;
    private float[] modelView;
    private float[] modelFloor;


    private static float MAX_TILT = 67.5f;
    private static float MIN_TILT = 0f;
    private static float INIT_TILT = MAX_TILT;
    private static float INIT_ZOOM = 15.5f;

    private static float MAX_BEARING = 90; // East
    private static float MIN_BEARING = -90; // West
    private static float INIT_BEARING = 0; // North

    // New York!
    private static float INIT_LAT = 40.7127f;
    private static float INIT_LONG = -74.0059f;

    private LatLng currentLatLng;

    private float cameraZoom;
    private float cameraTilt;
    private float cameraBearing;

    private int score = 0;
    private float objectDistance = 12f;
    private float floorDepth = 20f;

    private Vibrator vibrator;
    private CardboardOverlayView overlayView;

    private GoogleMap googleMap1;
    private GoogleMap googleMap2;
    private int mapType = GoogleMap.MAP_TYPE_SATELLITE;

    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;

    /**
     * Sets the view to our CardboardView and initializes the transformation matrices we will use
     * to render our scene.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();

        setContentView(R.layout.common_ui);
        CardboardView cardboardView = (CardboardView) findViewById(R.id.cardboard_view);
        cardboardView.setRestoreGLStateEnabled(false);
        cardboardView.setRenderer(this);
        setCardboardView(cardboardView);

        modelCube = new float[16];
        camera = new float[16];
        view = new float[16];
        modelViewProjection = new float[16];
        modelView = new float[16];
        modelFloor = new float[16];
        headView = new float[16];

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

//      overlayView = (CardboardOverlayView) findViewById(R.id.overlay);

        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment1 =  (MapFragment)
                fragmentManager.findFragmentById(R.id.map1);
        googleMap1 = mapFragment1.getMap();

        MapFragment mapFragment2 =  (MapFragment)
                fragmentManager.findFragmentById(R.id.map2);
        googleMap2 = mapFragment2.getMap();

        googleMap1.getUiSettings().setAllGesturesEnabled(false);
        googleMap1.getUiSettings().setCompassEnabled(false);
        googleMap1.getUiSettings().setMapToolbarEnabled(false);
        googleMap1.getUiSettings().setZoomControlsEnabled(false);

        googleMap2.getUiSettings().setAllGesturesEnabled(false);
        googleMap2.getUiSettings().setCompassEnabled(false);
        googleMap2.getUiSettings().setMapToolbarEnabled(false);
        googleMap2.getUiSettings().setZoomControlsEnabled(false);

        googleMap1.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap2.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap1.setBuildingsEnabled(true);
        googleMap2.setBuildingsEnabled(true);

        currentLatLng = new LatLng(INIT_LAT, INIT_LONG);
        cameraZoom = INIT_ZOOM;
        cameraTilt = INIT_TILT;
        cameraBearing = INIT_BEARING;

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(currentLatLng)
                .zoom(cameraZoom)
                .bearing(cameraBearing)
                .tilt(cameraTilt).build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        moveCamera(googleMap1, cameraUpdate, null);
        moveCamera(googleMap2, cameraUpdate, cameraCallback);
    }

    private GoogleMap.CancelableCallback cameraCallback = new GoogleMap.CancelableCallback() {
        @Override
        public void onFinish() {
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentLatLng = new LatLng(currentLatLng.latitude + 0.00006d, currentLatLng.longitude);
                        //CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(currentLatLng);

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(currentLatLng)
                                .zoom(cameraZoom)
                                .bearing(cameraBearing)
                                .tilt(cameraTilt).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);

                        moveCamera(googleMap1, cameraUpdate, null);
                        moveCamera(googleMap2, cameraUpdate, cameraCallback);
                    }
                }, 50);
        }

        @Override
        public void onCancel() {

        }
    };

    /**
     * Change the camera position by moving or animating the camera depending on the state of the
     * animate toggle button.
     */
    private void moveCamera(GoogleMap map, CameraUpdate update, GoogleMap.CancelableCallback callback) {
        map.animateCamera(update, 200, callback);
    }

    @Override
    public void onRendererShutdown() {
        Log.i(TAG, "onRendererShutdown");
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        Log.i(TAG, "onSurfaceChanged");
    }

    /**
     * Creates the buffers we use to store information about the 3D world.
     *
     * <p>OpenGL doesn't use Java arrays, but rather needs data in a format it can understand.
     * Hence we use ByteBuffers.
     *
     * @param config The EGL configuration used when creating the surface.
     */
    @Override
    public void onSurfaceCreated(EGLConfig config) {
        Log.i(TAG, "onSurfaceCreated");
    }
    private boolean setYaw = false;
    private float maxYaw;
    private float minYaw;
    /**
     * Prepares OpenGL ES before we draw a frame.
     *
     * @param headTransform The head transformation in the new frame.
     */
    @Override
    public void onNewFrame(HeadTransform headTransform) {
        //Log.i(TAG, "onNewFrame");
        float[] eulerAngles = new float[3];
        headTransform.getEulerAngles(eulerAngles, 0);
        float pitch = (float) Math.toDegrees(eulerAngles[0]) + 90;
        if(pitch>67.5f)
            pitch = 67.5f;
        float yaw = (float) Math.toDegrees(eulerAngles[1]);
        if(!setYaw)
        {
            maxYaw = yaw;
            minYaw = yaw;
            setYaw = true;
        }
        else
        {
            maxYaw = Math.max(maxYaw, yaw);
            minYaw = Math.min(minYaw, yaw);
        }
        cameraBearing = -1 * (yaw - 180);
        if (cameraBearing > 180)
            cameraBearing = cameraBearing - 360;
        else if (cameraBearing < - 180 )
            cameraBearing = cameraBearing + 360;

        cameraTilt = pitch;

        //CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        //moveCamera(googleMap1, cameraUpdate, null);
        //moveCamera(googleMap2, cameraUpdate, null);
    }

    /**
     * Draws a frame for an eye.
     *
     * @param eye The eye to render. Includes all required transformations.
     */
    @Override
    public void onDrawEye(Eye eye)
    {
//    GLES20.glEnable(GLES20.GL_DEPTH_TEST);
//    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
//
//    checkGLError("colorParam");
//
//    // Apply the eye transformation to the camera.
//    Matrix.multiplyMM(view, 0, eye.getEyeView(), 0, camera, 0);
//
//    // Set the position of the light
//    Matrix.multiplyMV(lightPosInEyeSpace, 0, view, 0, LIGHT_POS_IN_WORLD_SPACE, 0);
//
//    // Build the ModelView and ModelViewProjection matrices
//    // for calculating cube position and light.
//    float[] perspective = eye.getPerspective(Z_NEAR, Z_FAR);
//    Matrix.multiplyMM(modelView, 0, view, 0, modelCube, 0);
//    Matrix.multiplyMM(modelViewProjection, 0, perspective, 0, modelView, 0);
//    drawCube();
//
//    // Set modelView for the floor, so we draw floor in the correct location
//    Matrix.multiplyMM(modelView, 0, view, 0, modelFloor, 0);
//    Matrix.multiplyMM(modelViewProjection, 0, perspective, 0,
//      modelView, 0);
//    drawFloor();
    }

    @Override
    public void onFinishFrame(Viewport viewport) {
    }

    /**
     * Called when the Cardboard trigger is pulled.
     */
    @Override
    public void onCardboardTrigger() {
        Log.i(TAG, "onCardboardTrigger");

        // Always give user feedback.
        vibrator.vibrate(50);

        finish();
    }

    /**
     * Check if user is looking at object by calculating where the object is in eye-space.
     *
     * @return true if the user is looking at the object.
     */
    private boolean isLookingAtObject() {
        float[] initVec = { 0, 0, 0, 1.0f };
        float[] objPositionVec = new float[4];

        // Convert object space to camera space. Use the headView from onNewFrame.
        Matrix.multiplyMM(modelView, 0, headView, 0, modelCube, 0);
        Matrix.multiplyMV(objPositionVec, 0, modelView, 0, initVec, 0);

        float pitch = (float) Math.atan2(objPositionVec[1], -objPositionVec[2]);
        float yaw = (float) Math.atan2(objPositionVec[0], -objPositionVec[2]);

        return Math.abs(pitch) < PITCH_LIMIT && Math.abs(yaw) < YAW_LIMIT;
    }

    public void setupVoiceRecognition() {
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                "en-US");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);

        speech.startListening(recognizerIntent);
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.i(TAG, "onReadyForSpeech");
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.i(TAG, "onBeginningOfSpeech");
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.i(TAG, "onRmsChanged: " + rmsdB);
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(TAG, "onEndOfSpeech");
    }

    @Override
    public void onError(int error) {
        Log.e(TAG, getErrorText(error));

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                speech.startListening(recognizerIntent);
            }
        }, 1000);
    }

    @Override
    public void onResults(Bundle results) {
        Log.i(TAG, "onResults");
        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String match = matches.get(0);
        Log.d(TAG, match);
        if (match.equals("back")||match.equals("menu")||match.equals("бег"))
                finish();
        else {
//            speech.stopListening();
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                  speech.startListening(recognizerIntent);
                              }
                          }, 1000);
        }

    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.i(TAG, "onPartialResults");
    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.i(TAG, "onEvent");
    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null) {
            speech.stopListening();
            speech.destroy();
            Log.i(TAG, "speech destroyed!!!");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupVoiceRecognition();
    }
}
